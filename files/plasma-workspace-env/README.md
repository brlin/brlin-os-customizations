# plasma-workspace-env

Environment scripts for the KDE Plasma desktop environment.

## References

* [Session Environment Variables - KDE UserBase Wiki](https://userbase.kde.org/Session_Environment_Variables)
